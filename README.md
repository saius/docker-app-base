Primero instalamos x11docker localmente con el administrador de paquetes de tu distro 
o como dice en https://github.com/mviereck/x11docker#installation

Vamos a usar esta imagen de XFCE https://hub.docker.com/r/x11docker/xfce
(https://github.com/mviereck/dockerfile-x11docker-xfce)

Para probarla rápido localmente:

`x11docker --desktop --size 320x240 x11docker/lxde`

Para crear la imagen de este proyecto:

`docker build -t docker-app-base .`

Y la corremos con x11docker (con permisos para hacer `su`):

`x11docker --sudouser --desktop --size 800x600 docker-app-base`

(acá usamos la resolución 800x600 pero puede ser 1024x768 o cualquiera que quieran)

Además podemos ver el container andando:

`docker ps`

O conectarnos a su terminal:

`docker exec -it NOMBRE_CONTAINER bash`

Si queremos logearnos como root dentro del container primero hay que pasarle 
`--sudouser` al comando x11docker. Después ejecutar `su` dentro del container,
 contraseña "x11docker".

Tener en cuenta que cualquier modificación hecha al container una vez 
abierto va a ser descartada cuando cierren la ventana del XFCE o corten 
la ejecución del comando x11docker.

Para más imágenes de desktops environments que no sean XFCE visitar 
https://hub.docker.com/u/x11docker/
