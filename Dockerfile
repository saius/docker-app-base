FROM x11docker/xfce

# agregamos listas non-free cuando se requieran este tipo de dependencias
#RUN echo 'deb http://ftp.de.debian.org/debian buster main non-free' >> /etc/apt/sources.list

# updateamos listas e instalamos utilidades de debugeo por si acaso
RUN apt-get update
RUN apt-get install -y curl nano procps lsof locate htop jq nomacs

## si quisieramos intalar desde un .deb
# copiar archivos necesarios (previamente descargados)
#COPY ARCHIVO.deb /ARCHIVO.deb
# instalar paquetes
#RUN apt-get install -yf /ARCHIVO.deb

## acá poner la config de la shell
# mensaje sobre contraseña de root (cuando no sos root)
RUN echo '[ ! `whoami` = root ] && echo "su para root. contrasenia: x11docker"' >> /etc/bash.bashrc
